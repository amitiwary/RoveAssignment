package com.example.meeera.roveassignment.Rest;

import com.example.meeera.roveassignment.Model.ContributorResponse;
import com.example.meeera.roveassignment.Model.RepoItems;
import com.example.meeera.roveassignment.Model.TopRepoResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import retrofit2.http.Url;

/**
 * Created by meeera on 9/6/18.
 */

public interface RoveService {

    @GET
    Call<TopRepoResponse> getTopRepo(@Url String url);

    @GET
    Call<List<ContributorResponse>> getContributors(@Url String url);

    @GET
    Call<List<RepoItems>> getContributorRepoResponse(@Url String url);
}
