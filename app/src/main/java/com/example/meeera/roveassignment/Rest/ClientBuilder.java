package com.example.meeera.roveassignment.Rest;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by meeera on 9/6/18.
 */

//this class is used to initialize retrofit service.
public class ClientBuilder {
    private static Retrofit retrofit;
    private static RoveService roveService;

    public ClientBuilder() {
        createRevoService();
    }

    public static void init(){
        roveService = createApi(RoveService.class);
    }

    private static <T> T createApi(Class<T> clazz) {
        return retrofit.create(clazz);
    }

    public static void createRevoService(){
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);

        try{
            retrofit = new Retrofit.Builder()
                       .baseUrl("https://api.github.com")
                       .addConverterFactory(GsonConverterFactory.create())
                       .client(httpClient.build())
                       .build();
            init();
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public RoveService getRoveService(){
        return roveService;
    }
}
