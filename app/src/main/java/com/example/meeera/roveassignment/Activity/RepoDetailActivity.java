package com.example.meeera.roveassignment.Activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.example.meeera.roveassignment.Adapter.RepoAdapter;
import com.example.meeera.roveassignment.Model.ContributorResponse;
import com.example.meeera.roveassignment.R;
import com.example.meeera.roveassignment.Rest.ClientBuilder;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by meeera on 9/6/18.
 */

//this activity contain information about repository and all the contributor of that repository

public class RepoDetailActivity extends AppCompatActivity implements RepoAdapter.onClick {

    @BindView(R.id.project_name)
    TextView projectName;

    @BindView(R.id.project_link)
    TextView projectLink;

    @BindView(R.id.description)
    TextView projectDescription;

    @BindView(R.id.contributor)
    TextView contributorTxt;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.lottie_anim)
    LottieAnimationView lottieAnimationView;

    @BindView(R.id.try_again)
    TextView tryAgain;

    ClientBuilder clientBuilder;
    List<ContributorResponse> contributorResponseList;
    ProgressDialog progressDialog;
    CustomTabsIntent customTabsIntent;
    CustomTabsIntent.Builder customTabBuilder;
    String url = "";
    private static final String TAG = "CustomTabsHelper";
    static final String STABLE_PACKAGE = "com.android.chrome";
    static final String BETA_PACKAGE = "com.chrome.beta";
    static final String DEV_PACKAGE = "com.chrome.dev";
    static final String LOCAL_PACKAGE = "com.google.android.apps.chrome";
    private static final String ACTION_CUSTOM_TABS_CONNECTION =
            "android.support.customtabs.action.CustomTabsService";

    private static String sPackageNameToUse;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.repo_detail_activity);
        ButterKnife.bind(this);
        lottieAnimationView.setAnimation("network_lost.json");
        lottieAnimationView.loop(true);
        progressDialog = new ProgressDialog(RepoDetailActivity.this);
        progressDialog.setMessage("Please Wait ..");
        customTabBuilder = new CustomTabsIntent.Builder();
        customTabBuilder.setToolbarColor(getResources().getColor(R.color.colorPrimary));
        customTabsIntent = customTabBuilder.build();
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            url = bundle.getString("url");
            String name = bundle.getString("name");
            final String htmlUrl = bundle.getString("htmlurl");
            String description = bundle.getString("description");
            int activityInfo = bundle.getInt("activity_info");
            Log.d("url ", "url" + url);
            String packageName = getPackageNameToUse(RepoDetailActivity.this);
            if (packageName != null) {
                //it will open url directly in chrome custom tabs rather than asking user for browser.
                customTabsIntent.intent.setPackage(packageName);
            }
            projectName.setText(name);
            projectLink.setText(htmlUrl);
            projectDescription.setText(description);
            projectLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    customTabsIntent.launchUrl(RepoDetailActivity.this, Uri.parse(htmlUrl));
                }
            });
            tryAgain.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    closeAnim();
                    getData();
                }
            });
            //if activity_info is 1 then this activity is called from MainActivity else from ContributorDetailActivity.
            //If it is called from MainActivity we are showing list of contributors otherwise not.
            if (activityInfo == 1) {
                contributorTxt.setVisibility(View.VISIBLE);
                clientBuilder = new ClientBuilder();
                //network call to get all contributors of repository
                getData();
            } else {
                contributorTxt.setVisibility(View.GONE);
                progressDialog.dismiss();
            }
        } else {
            progressDialog.dismiss();
            Toast.makeText(RepoDetailActivity.this, "Some error occurs", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void getData(){
        progressDialog.show();
        clientBuilder.getRoveService().getContributors(url).enqueue(new Callback<List<ContributorResponse>>() {
            @Override
            public void onResponse(Call<List<ContributorResponse>> call, Response<List<ContributorResponse>> response) {
                contributorResponseList = response.body();
                Log.d("list ", "size " + contributorResponseList.size());
                setRecyclerView(contributorResponseList);
            }

            @Override
            public void onFailure(Call<List<ContributorResponse>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(RepoDetailActivity.this, "Some error occurs", Toast.LENGTH_SHORT).show();
                Log.d("fail ", "failure" + t.getMessage());
                showAnim();
            }
        });
    }

    public void setRecyclerView(List<ContributorResponse> contributorResponseList) {
        progressDialog.dismiss();
        recyclerView.setAdapter(new RepoAdapter(RepoDetailActivity.this, contributorResponseList, this));
        recyclerView.setLayoutManager(new GridLayoutManager(RepoDetailActivity.this, 2));
    }

    public static String getPackageNameToUse(Context context) {
        if (sPackageNameToUse != null) return sPackageNameToUse;

        PackageManager pm = context.getPackageManager();
        // Get default VIEW intent handler.
        Intent activityIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.example.com"));
        ResolveInfo defaultViewHandlerInfo = pm.resolveActivity(activityIntent, 0);
        String defaultViewHandlerPackageName = null;
        if (defaultViewHandlerInfo != null) {
            defaultViewHandlerPackageName = defaultViewHandlerInfo.activityInfo.packageName;
        }

        // Get all apps that can handle VIEW intents.
        List<ResolveInfo> resolvedActivityList = pm.queryIntentActivities(activityIntent, 0);
        List<String> packagesSupportingCustomTabs = new ArrayList<>();
        for (ResolveInfo info : resolvedActivityList) {
            Intent serviceIntent = new Intent();
            serviceIntent.setAction(ACTION_CUSTOM_TABS_CONNECTION);
            serviceIntent.setPackage(info.activityInfo.packageName);
            if (pm.resolveService(serviceIntent, 0) != null) {
                packagesSupportingCustomTabs.add(info.activityInfo.packageName);
            }
        }

        // Now packagesSupportingCustomTabs contains all apps that can handle both VIEW intents
        // and service calls.
        if (packagesSupportingCustomTabs.isEmpty()) {
            sPackageNameToUse = null;
        } else if (packagesSupportingCustomTabs.size() == 1) {
            sPackageNameToUse = packagesSupportingCustomTabs.get(0);
        } else if (!TextUtils.isEmpty(defaultViewHandlerPackageName)
                && !hasSpecializedHandlerIntents(context, activityIntent)
                && packagesSupportingCustomTabs.contains(defaultViewHandlerPackageName)) {
            sPackageNameToUse = defaultViewHandlerPackageName;
        } else if (packagesSupportingCustomTabs.contains(STABLE_PACKAGE)) {
            sPackageNameToUse = STABLE_PACKAGE;
        } else if (packagesSupportingCustomTabs.contains(BETA_PACKAGE)) {
            sPackageNameToUse = BETA_PACKAGE;
        } else if (packagesSupportingCustomTabs.contains(DEV_PACKAGE)) {
            sPackageNameToUse = DEV_PACKAGE;
        } else if (packagesSupportingCustomTabs.contains(LOCAL_PACKAGE)) {
            sPackageNameToUse = LOCAL_PACKAGE;
        }
        return sPackageNameToUse;
    }

    private static boolean hasSpecializedHandlerIntents(Context context, Intent intent) {
        try {
            PackageManager pm = context.getPackageManager();
            List<ResolveInfo> handlers = pm.queryIntentActivities(
                    intent,
                    PackageManager.GET_RESOLVED_FILTER);
            if (handlers == null || handlers.size() == 0) {
                return false;
            }
            for (ResolveInfo resolveInfo : handlers) {
                IntentFilter filter = resolveInfo.filter;
                if (filter == null) continue;
                if (filter.countDataAuthorities() == 0 || filter.countDataPaths() == 0) continue;
                if (resolveInfo.activityInfo == null) continue;
                return true;
            }
        } catch (RuntimeException e) {
            Log.e(TAG, "Runtime exception while getting specialized handlers");
        }
        return false;
    }

    public void showAnim(){
        lottieAnimationView.setVisibility(View.VISIBLE);
        lottieAnimationView.playAnimation();
        tryAgain.setVisibility(View.VISIBLE);
    }

    public void closeAnim(){
        lottieAnimationView.pauseAnimation();
        lottieAnimationView.setVisibility(View.GONE);
        tryAgain.setVisibility(View.GONE);
    }

    @Override
    public void clickOn(String name, String imgUrl, String repos) {
        Intent intent = new Intent(RepoDetailActivity.this, ContributorDetailActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("imgurl", imgUrl);
        intent.putExtra("repos", repos);
        startActivity(intent);
    }
}
