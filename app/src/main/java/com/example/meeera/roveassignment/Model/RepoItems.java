package com.example.meeera.roveassignment.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by meeera on 9/6/18.
 */

public class RepoItems {
    @SerializedName("name")
    @Expose
    String name;

    @SerializedName("full_name")
    @Expose
    String fullName;

    @SerializedName("html_url")
    @Expose
    String htmlUrl;

    @SerializedName("url")
    @Expose
    String url;

    @SerializedName("description")
    @Expose
    String description;

    @SerializedName("contributors_url")
    String contributorsUrl;

    @SerializedName("owner")
    @Expose
    TopRepoOwner topRepoOwner;

    public TopRepoOwner getTopRepoOwner() {
        return topRepoOwner;
    }

    public String getName() {
        return name;
    }

    public String getFullName() {
        return fullName;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getDescription() {
        return description;
    }

    public String getContributorsUrl() {
        return contributorsUrl;
    }

    public String getUrl() {
        return url;
    }
}
