package com.example.meeera.roveassignment.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by meeera on 9/6/18.
 */

public class TopRepoResponse {
    @SerializedName("items")
    @Expose
    List<RepoItems> items = new ArrayList<>();

    public List<RepoItems> getItems() {
        return items;
    }
}
