package com.example.meeera.roveassignment.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.bumptech.glide.Glide;
import com.example.meeera.roveassignment.Adapter.ContributorAdapter;
import com.example.meeera.roveassignment.Model.RepoItems;
import com.example.meeera.roveassignment.R;
import com.example.meeera.roveassignment.Rest.ClientBuilder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by meeera on 9/6/18.
 */

/*
* This activity contains information about contributor and his/her repository on github.
* */
public class ContributorDetailActivity extends AppCompatActivity implements ContributorAdapter.onClick{
    @BindView(R.id.card_view)
    CardView cardView;

    @BindView(R.id.img)
    ImageView imageView;

    @BindView(R.id.txtView)
    TextView textView;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.try_again)
    TextView tryAgain;

    @BindView(R.id.lottie_anim)
    LottieAnimationView lottieAnimationView;

    ClientBuilder clientBuilder;
    List<RepoItems> repoItemsList;
    ProgressDialog progressDialog;
    String url = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contributor_detail_activity);
        ButterKnife.bind(this);
        lottieAnimationView.setAnimation("network_lost.json");
        lottieAnimationView.loop(true);
        progressDialog = new ProgressDialog(ContributorDetailActivity.this);
        progressDialog.setMessage("please wait ..");
        Bundle bundle = getIntent().getExtras();
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAnim();
                getData();
            }
        });

        if(bundle != null) {
            url = bundle.getString("repos");
            String name = bundle.getString("name");
            String imageUrl = bundle.getString("imgurl");
            textView.setText(name);
            Glide.with(this).load(imageUrl).into(imageView);
            clientBuilder = new ClientBuilder();
            //network call to retrieve all the repo of the user.
           getData();
        }else{
            progressDialog.dismiss();
            Toast.makeText(ContributorDetailActivity.this, "Some error occurs", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void getData(){
        progressDialog.show();
        clientBuilder.getRoveService().getContributorRepoResponse(url).enqueue(new Callback<List<RepoItems>>() {
            @Override
            public void onResponse(Call<List<RepoItems>> call, Response<List<RepoItems>> response) {
                repoItemsList = response.body();
                Log.d("list ", "size "+repoItemsList.size());
                setRecyclerView(repoItemsList);
            }

            @Override
            public void onFailure(Call<List<RepoItems>> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ContributorDetailActivity.this, "Some error occurs", Toast.LENGTH_SHORT).show();
                Log.d("fail ", "failure"+t.getMessage());
                showAnim();
            }
        });
    }

    public void setRecyclerView(List<RepoItems> repoItemsList){
        progressDialog.dismiss();
        recyclerView.setAdapter(new ContributorAdapter(ContributorDetailActivity.this, repoItemsList, this));
        recyclerView.setLayoutManager(new GridLayoutManager(ContributorDetailActivity.this, 2));
    }

    public void showAnim(){
        lottieAnimationView.setVisibility(View.VISIBLE);
        lottieAnimationView.playAnimation();
        tryAgain.setVisibility(View.VISIBLE);
    }

    public void closeAnim(){
        lottieAnimationView.pauseAnimation();
        lottieAnimationView.setVisibility(View.GONE);
        tryAgain.setVisibility(View.GONE);
    }

    @Override
    public void clickOn(String name, String htmlUrl, String description, String url) {
        Intent intent = new Intent(ContributorDetailActivity.this, RepoDetailActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("htmlurl", htmlUrl);
        intent.putExtra("description", description);
        intent.putExtra("url", url);
        intent.putExtra("activity_info", 0);
        startActivity(intent);
    }
}
