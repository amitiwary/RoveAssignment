package com.example.meeera.roveassignment.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.meeera.roveassignment.Model.ContributorResponse;
import com.example.meeera.roveassignment.R;

import java.util.List;

/**
 * Created by meeera on 9/6/18.
 */

//adapter for RecyclerView to show list of all contributor of a repo.
public class RepoAdapter extends RecyclerView.Adapter<RepoAdapter.viewHolder>{
    Context context;
    List<ContributorResponse> contributorResponseList;
    onClick onClick;

    public RepoAdapter(Context context, List<ContributorResponse> contributorResponseList, RepoAdapter.onClick onClick) {
        this.context = context;
        this.contributorResponseList = contributorResponseList;
        this.onClick = onClick;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.repo_item, parent, false);
        viewHolder viewHolder = new viewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(viewHolder holder, final int position) {
        holder.textView.setText(contributorResponseList.get(position).getUserName());
        Glide.with(context).load(contributorResponseList.get(position).getUserImage()).into(holder.imageView);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.clickOn(contributorResponseList.get(position).getUserName(), contributorResponseList.get(position).getUserImage(), contributorResponseList.get(position).getReposUrl());
            }
        });
    }

    @Override
    public int getItemCount() {
        return contributorResponseList.size();
    }

    class viewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        ImageView imageView;
        TextView textView;
        public viewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            imageView = itemView.findViewById(R.id.img);
            textView = itemView.findViewById(R.id.txtView);
        }
    }

    public interface onClick{
        void clickOn(String name, String imgUrl, String repos);
    }

}
