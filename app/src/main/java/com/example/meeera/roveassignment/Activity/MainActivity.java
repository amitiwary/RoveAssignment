package com.example.meeera.roveassignment.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.example.meeera.roveassignment.Adapter.TopRepoAdapter;
import com.example.meeera.roveassignment.Model.RepoItems;
import com.example.meeera.roveassignment.Model.TopRepoResponse;
import com.example.meeera.roveassignment.R;
import com.example.meeera.roveassignment.Rest.ClientBuilder;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


//this activity shows top 10 repository sort by "watcher_count"
public class MainActivity extends AppCompatActivity implements TopRepoAdapter.onClicked {

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;

    @BindView(R.id.try_again)
    TextView tryAgain;

    @BindView(R.id.lottie_anim)
    LottieAnimationView lottieAnimationView;

    List<RepoItems> list;
    ProgressDialog progressDialog;
    ClientBuilder clientBuilder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        lottieAnimationView.setAnimation("network_lost.json");
        lottieAnimationView.loop(true);
        progressDialog = new ProgressDialog(MainActivity.this);
        progressDialog.setMessage("Please Wait ..");
        clientBuilder = new ClientBuilder();
        getData();
        tryAgain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                closeAnim();
                getData();
            }
        });
    }

    public void getData(){
        progressDialog.show();
        clientBuilder.getRoveService().getTopRepo("https://api.github.com/search/repositories?q=stars%3A%3E0&sort=watchers_count&per_page=10").enqueue(new Callback<TopRepoResponse>() {
            @Override
            public void onResponse(Call<TopRepoResponse> call, Response<TopRepoResponse> response) {
                list = response.body().getItems();
                Log.d("list ", "size "+list.size()+ " "+list.get(0).getFullName());
                setRecyclerView(list);
            }

            @Override
            public void onFailure(Call<TopRepoResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MainActivity.this, "Some error occurs", Toast.LENGTH_SHORT).show();
                Log.d("fail ", "failure"+t.getMessage());
                showAnim();
            }
        });
    }

    public void setRecyclerView(List<RepoItems> repoItemsList){
        progressDialog.dismiss();
        recyclerView.setAdapter(new TopRepoAdapter(MainActivity.this, repoItemsList, this));
        recyclerView.setLayoutManager(new GridLayoutManager(MainActivity.this, 2));
    }

    public void showAnim(){
        lottieAnimationView.setVisibility(View.VISIBLE);
        lottieAnimationView.playAnimation();
        tryAgain.setVisibility(View.VISIBLE);

    }

    public void closeAnim(){
        lottieAnimationView.pauseAnimation();
        lottieAnimationView.setVisibility(View.GONE);
        tryAgain.setVisibility(View.GONE);
    }

    @Override
    public void clickOn(String name, String htmlUrl, String description, String url) {
        Intent intent = new Intent(MainActivity.this, RepoDetailActivity.class);
        intent.putExtra("name", name);
        intent.putExtra("htmlurl", htmlUrl);
        intent.putExtra("description", description);
        intent.putExtra("url", url);
        intent.putExtra("activity_info", 1);
        startActivity(intent);
    }
}
