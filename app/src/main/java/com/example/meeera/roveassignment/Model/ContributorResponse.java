package com.example.meeera.roveassignment.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by meeera on 9/6/18.
 */

public class ContributorResponse {
    @SerializedName("login")
    @Expose
    String userName;

    @SerializedName("avatar_url")
    @Expose
    String userImage;

    @SerializedName("repos_url")
    @Expose
    String reposUrl;

    public String getUserName() {
        return userName;
    }

    public String getUserImage() {
        return userImage;
    }

    public String getReposUrl() {
        return reposUrl;
    }
}
