package com.example.meeera.roveassignment.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.meeera.roveassignment.Model.RepoItems;
import com.example.meeera.roveassignment.R;

import java.util.List;

/**
 * Created by meeera on 9/6/18.
 */

//adapter for RecyclerView to show list of top 10 repo.
public class TopRepoAdapter extends RecyclerView.Adapter<TopRepoAdapter.viewHolder>{

    Context context;
    List<RepoItems> list;
    onClicked onClicked;

    public TopRepoAdapter(Context context, List<RepoItems> list, onClicked onClicked) {
        this.context = context;
        this.list = list;
        this.onClicked = onClicked;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.top_repo_item, parent, false);
        viewHolder viewHolder = new viewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(viewHolder holder, final int position) {
        holder.textView.setText(list.get(position).getName());
        Glide.with(context).load(list.get(position).getTopRepoOwner().getAvatarUrl()).into(holder.imageView);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClicked.clickOn(list.get(position).getName(), list.get(position).getHtmlUrl(), list.get(position).getDescription(), list.get(position).getContributorsUrl());
            }

        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class viewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        TextView textView;
        ImageView imageView;
        public viewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            textView = itemView.findViewById(R.id.txtView);
            imageView = itemView.findViewById(R.id.img);
        }
    }

    public interface onClicked{
        void clickOn(String name, String htmlUrl, String description, String url);
    }

}
