package com.example.meeera.roveassignment.Adapter;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.example.meeera.roveassignment.Model.RepoItems;
import com.example.meeera.roveassignment.R;

import java.util.List;

/**
 * Created by meeera on 9/6/18.
 */

//adapter for RecyclerView to show list of repo of a user.
public class ContributorAdapter extends RecyclerView.Adapter<ContributorAdapter.viewHolder>{
    Context context;
    List<RepoItems> repoItemsList;
    onClick onClick;

    public ContributorAdapter(Context context, List<RepoItems> repoItemsList, ContributorAdapter.onClick onClick) {
        this.context = context;
        this.repoItemsList = repoItemsList;
        this.onClick = onClick;
    }

    @Override
    public viewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.top_repo_item, parent, false);
        viewHolder viewHolder = new viewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(viewHolder holder, final int position) {
        holder.textView.setText(repoItemsList.get(position).getName());
        Glide.with(context).load(repoItemsList.get(position).getTopRepoOwner().getAvatarUrl()).into(holder.imageView);
        holder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.clickOn(repoItemsList.get(position).getName(), repoItemsList.get(position).getHtmlUrl(), repoItemsList.get(position).getDescription(), repoItemsList.get(position).getUrl());
            }

        });
    }

    @Override
    public int getItemCount() {
        return repoItemsList.size();
    }

    class viewHolder extends RecyclerView.ViewHolder{

        CardView cardView;
        TextView textView;
        ImageView imageView;
        public viewHolder(View itemView) {
            super(itemView);
            cardView = itemView.findViewById(R.id.card_view);
            textView = itemView.findViewById(R.id.txtView);
            imageView = itemView.findViewById(R.id.img);
        }
    }

    public interface onClick{
        void clickOn(String name, String htmlUrl, String description, String url);
    }
}
