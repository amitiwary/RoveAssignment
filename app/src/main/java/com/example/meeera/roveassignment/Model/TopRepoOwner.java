package com.example.meeera.roveassignment.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by meeera on 20/6/18.
 */

public class TopRepoOwner {
    @SerializedName("avatar_url")
    @Expose
    public String avatarUrl;

    public String getAvatarUrl() {
        return avatarUrl;
    }
}
